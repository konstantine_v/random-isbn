require "./rand"
require "json"

class ISBN::Generate
  LANG_ENG = ["9780", "9781"] #This is the array that'll be used for the english one, if there's more I can add it below, I'm sure there's a better way of doing it.
  LANG = LANG_ENG.sample()    #Pick randomly from the array above and

  # Macros for creating the ISBNs quicker
  # NOTE: Need more work to get running but the concept is there
  macro isbn_gen_type_1(byte, rand_num)
    rand_byte = RandNum.fromZero {{byte}}
    reg_group = Random.new.rand({{rand_num}})
  end

  # For Nested Arrays
  macro isbn_gen_type_2(byte, group_arr)
    rand_byte = RandNum.fromZero {{byte}}
    reg_group = Random.new.rand( {{group_arr}}.sample() )
  end

  def self.english
    if (LANG === "9780")
      digit_codes = Random.new.rand(1..6)
      case digit_codes
      when 6
        isbn_gen_type_1(7,1)
      when 5
        isbn_gen_type_2(6,[200..227, 229..368, 370..638, 640..647, 649..654, 656..699])
      when 4
        isbn_gen_type_2(5,[2280..2289, 3690..3699, 6390..6398, 6550..6559, 7000..8499])
      when 3
        isbn_gen_type_1(4,85000..89999)
      when 2
        isbn_gen_type_1(7,900000..949999)
      when 1
        isbn_gen_type_2(2,[6399000..6399999, 6480000..6489999, 9500000..9999999])
      else nil
      end
    end

    # This will be all the logic for the English 9781 reference group for ISBNs, mostly the same as above.
    if (LANG === "9781")
      digit_codes = Random.new.rand(5..6)

      # Check if the digit_codes match and generate the rest of the numbers accordingly
      case digit_codes
      when 6
        isbn_gen_type_1(7,1..6)
      when 5
        rand_byte = RandNum.fromZero 6
        reg_group_rand = [0..9, 100..397, 714..716].sample()
        reg_group = Random.new.rand(reg_group_rand)
        if (reg_group < 10) #Todo: Cleanup by making this a ternary to be cleaner
          reg_group = "00" + reg_group.to_s
        end
      when 4
        rand_byte = RandNum.fromZero 5
        reg_group_rand = [700..999, 3980..5499, 6860..7139, 7170..7319, 7900..7999, 8672..8675, 9730..9877].sample()
        reg_group = Random.new.rand(reg_group_rand)
        if (reg_group > 700 && reg_group < 999) #Todo: Cleanup by making this a ternary to be cleaner
          reg_group = "0" + reg_group.to_s
        end
      when 3
        isbn_gen_type_2(7,[55000..68599, 74000..77499, 77540..77639, 77650..77699, 77770..78999, 80000..86719, 86760..86979])
      when 2
        isbn_gen_type_1(7,1)
      when 1
        isbn_gen_type_2(7,[7320000..7399999, 7750000..7753999, 7764000..7764999, 7770000..7776999, 9160000..9165059, 9990000..9999999])
      else nil
      end
    end
    
    # Returns what is needed as a string
    return LANG + reg_group.to_s + rand_byte.to_s
  end
end
